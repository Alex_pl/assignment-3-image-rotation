//
// Created by Александр Плетнёв on 20.11.2022.
//

#ifndef IMAGE_TRANSFORMER_ROTATE_H
#define IMAGE_TRANSFORMER_ROTATE_H

#include "bmp.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct image rotate( struct image const source );

#endif //IMAGE_TRANSFORMER_ROTATE_H
