//
// Created by Александр Плетнёв on 21.11.2022.
//

#ifndef IMAGE_TRANSFORMER_WRITE_OUTPUT_UTIL_H
#define IMAGE_TRANSFORMER_WRITE_OUTPUT_UTIL_H

#include <stdio.h>

void print_success(char* s);
void print_err(char* s);

#endif//IMAGE_TRANSFORMER_WRITE_OUTPUT_UTIL_H
