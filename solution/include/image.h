//
// Created by Александр Плетнёв on 20.11.2022.
//

#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H


#include <stdint.h>
#include <stdio.h>


/**just a <b>pixel</b>*/
#pragma pack(push, 1)
struct pixel { uint8_t b, g, r; };
#pragma pack(pop)

/**just a <b>image</b>*/
#pragma pack(push, 1)
struct image {
    uint64_t width, height;
    struct pixel* data;
};
#pragma pack(pop)

enum open_status {
    OPEN_OK = 0,
    OPEN_ERR
};

enum close_status {
    CLOSE_OK = 0,
    CLOSE_ERR
};

enum open_status open_file(FILE** file, char*  path, char* mode);
enum close_status close_file(FILE** file);
#endif //IMAGE_TRANSFORMER_IMAGE_H
