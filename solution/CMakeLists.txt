file(GLOB_RECURSE sources CONFIGURE_DEPENDS
    src/*.c
    src/*.h
    include/*.h
)

add_executable(image-transformer ${sources} src/bmp.c include/bmp.h src/image.c include/image.h src/rotate.c include/rotate.h src/write_output_util.c include/write_output_util.h)
target_include_directories(image-transformer PRIVATE src include)
