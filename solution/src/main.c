#include "./../include/bmp.h"
#include "./../include/rotate.h"
#include "./../include/write_output_util.h"
int main(int argc, char** argv ) {
    /**
     * checking the number of arguments
     */
    if (argc != 3) {
        print_err("invalid number or format of arguments (correct format: ./image-transformer <source-image> <transformed-image>)");
        return 1;
    }
    /**
     * set paths
     */
    char *inputPath = argv[1];
    char *outputPath = argv[2];
    /**
     * try open file and fill <code>*input</code>
     */
    FILE *input = NULL;
    //открываем
    enum open_status openInputStatus = open_file(&input, inputPath, "rb");
    if (openInputStatus == 1) {
        print_err("can't open input file, check your input, pls");
        return 1;
    } else print_success("input file opened successfully");

    struct image img = {0};
    //читаем
    enum read_status readStatus = from_bmp(input, &img);
    if (readStatus == READ_INVALID_HEADER) {
        print_err("read invalid header, check your file, pls");
        return 1;
    } else if (readStatus) {
        print_err("can't read file, check your file, pls");
        return 1;
    } else print_success("file read successfully");

    //закрываем
    enum close_status closeInputStatus = close_file(&input);
    if (closeInputStatus) {
        print_err("can't close input file ...");
        return 1;
    } else print_success("input file closed successfully");

    //переворачиваем
    struct image newImage = rotate(img);



    FILE *output = NULL;
    //открываем
    enum open_status openOutputStatus = open_file(&output, outputPath, "wb");
    if (openOutputStatus) {
        print_err("can't open output file, check your input, pls");
        return 1;
    } else print_success("output file opened successfully");

    //пишем
    enum write_status writeStatus = to_bmp(output, &newImage);
    if (writeStatus) {
        fprintf(stderr, "%d :", writeStatus);
        print_err("can't write in your file");
        return 1;
    } else print_success("write in file successfully");

    //закрываем
    enum close_status closeOutputStatus = close_file(&output);
    if (closeOutputStatus) {
        print_err("can't close output file ...");
        return 1;
    } else print_success("output file closed successfully");
    
    return 0;
}
