//
// Created by Александр Плетнёв on 20.11.2022.
//

#include "./../include/bmp.h"
/**
 * constant name form <a href="https://ru.wikipedia.org/wiki/BMP">wiki</a>
 */
#define LITTLE_ENDIAN_BMP 0x4D42
#define BIG_ENDIAN_BMP 0x4D42
#define RESERVED_VALUE 0
#define BITMAPINFOHEADER 40
#define BITMAPV4HEADER 108
#define BITMAPV5HEADER 124
#define BI_PLANES_BMP 1
#define BIT_COUNT 24
#define BI_RGB 0
#define X_PELS_PER_METER 2835
#define Y_PELS_PER_METER 2835
#define CLR_USED 0

#pragma pack(push, 1)
struct bmp_header {
uint16_t bfType;
uint32_t bfileSize;
uint32_t bfReserved;
uint32_t bOffBits;
uint32_t biSize;
uint32_t biWidth;
uint32_t biHeight;
uint16_t biPlanes;
uint16_t biBitCount;
uint32_t biCompression;
uint32_t biSizeImage;
uint32_t biXPelsPerMeter;
uint32_t biYPelsPerMeter;
uint32_t biClrUsed;
uint32_t biClrImportant;
};
#pragma pack(pop)

/**
 * return padding size for img
 * @param img - struct image*
 * @return size of padding
 */
static uint32_t get_padding(struct image const* img) {
    if (img->width * sizeof(struct pixel) % 4 == 0) return 0;
    return 4 - img->width * sizeof(struct pixel) % 4;
}
/**
 * create bmp header for img
 * @param img - struct image*
 * @return bmp header (struct bmp_header)
 */
static struct bmp_header make_bmp_header(struct image const* img) {
    struct bmp_header bmpHeader = {
            .bfType = LITTLE_ENDIAN_BMP,
            .bfileSize = img->width * img->height * sizeof(struct pixel)
                         + img->height * get_padding(img) * sizeof(struct pixel)
                         + sizeof(struct bmp_header),
            .bfReserved = RESERVED_VALUE,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = BITMAPINFOHEADER,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = BI_PLANES_BMP,
            .biBitCount = BIT_COUNT,
            .biCompression = BI_RGB,
            .biSizeImage = img->width * img->height * sizeof(struct pixel)
                           + img->height * get_padding(img) * sizeof(struct pixel),
            .biXPelsPerMeter = X_PELS_PER_METER,
            .biYPelsPerMeter = Y_PELS_PER_METER,
            .biClrUsed = CLR_USED,
            .biClrImportant = CLR_USED
    };
    return bmpHeader;
}
static struct image allocate_image(struct bmp_header bmpHeader) {
    return (struct image) {bmpHeader.biWidth,
                            bmpHeader.biHeight,
                            malloc(bmpHeader.biSizeImage)};
}
void free_image(struct image const* img) {
    free(img->data);
}

enum read_status from_bmp( FILE *in, struct image *img ) {
    struct bmp_header bmpHeader = {0};
    //size_t headerSize =
    if (fread(&bmpHeader,  sizeof(struct bmp_header), 1, in) != 1) return READ_INVALID_HEADER;
    //TODO: validate header
    *img = allocate_image(bmpHeader);
    uint32_t padding = get_padding(img);
    for (size_t row = 0; row < img->height; row++) {
        size_t pxRead = fread(&(img->data[row*img->width]), sizeof(struct pixel), img->width, in);
        if (fseek(in, padding, SEEK_CUR) || pxRead != img->width) {
            free_image(img);
            return READ_INVALID_SIGNATURE;
        }
    }
    return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img) {
    struct bmp_header header = make_bmp_header(img);
    if (!fwrite(&header, sizeof(struct bmp_header), 1, out)) {
        free_image(img);
        return WRITE_HEADER_ERROR;
    }

    uint32_t padding = get_padding(img);

    for (size_t i = 0; i < img->height; i++) {
        if (fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out) != img->width) {
            free_image(img);
            return WRITE_DATA_ERROR;
        }

        if (fseek(out, padding, SEEK_CUR) != 0) {
            free_image(img);
            return WRITE_PADDING_ERROR;
        }
    }
    free_image(img);
    return WRITE_OK;
}


