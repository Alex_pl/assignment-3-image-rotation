//
// Created by Александр Плетнёв on 20.11.2022.
//

#include "./../include/image.h"

enum open_status open_file(FILE** file, char*  path, char* mode) {
    *file = fopen(path, mode);
    if (!*file) {
        return OPEN_ERR;
    }
    return OPEN_OK;
}

enum close_status close_file(FILE** file) {
    //int state = fclose(*file);
     if (!*file) {
         return CLOSE_ERR;
     }
     fclose(*file);
     return CLOSE_OK;
}
