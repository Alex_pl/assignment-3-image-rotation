//
// Created by Александр Плетнёв on 20.11.2022.
//

#include "./../include/rotate.h"

/**
 * rotate image on 90 degrees
 * @warning allocate mem for return struct image->data
 * @param source - input struct image
 * @return rotated new struct image
 */
struct image rotate(struct image const source) {
    struct image newImg = {source.height, source.width, 0};
    newImg.data = malloc(sizeof(struct pixel) * newImg.width * newImg.height);

    for (size_t i = 0; i < source.width; i++) {
        for (size_t j = 0; j < source.height; j++) {
            newImg.data[i * newImg.width + j] = source.data[(source.height - j - 1) * source.width + i];//[(source.width - i - 1) * source.height + j];
        }
    }
    free_image(&source);
    return newImg;
}
